<?php

 /*
 Template Name: Condominium Page
 Template Post Type: post, page, event
 */
 // Page code here...
 get_header();
 ?>
 <nav class="nextNav active navbar navbar-default mega trans-helper navbar-trans navbar-trans-dark row">
	 <div class="container">
		 <div class="navbar-header">
			 <button id="nav-icon1" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			 <span class="sr-only">Toggle navigation</span>
			 <span class="icon-bar"></span>
			 <span class="icon-bar"></span>
			 <span class="icon-bar"></span>
			 </button>
			 <!-- Logo -->
			 <a class="navbar-brand" href="index.html">
				 <img src="<?php echo site_url(). '/wp-content/uploads/2018/04/Aeon-Towers-logo-1.png'; ?>" alt="Definity - Logo">
			 </a>
		 </div><!-- / .navbar-header -->
		 <!-- Navbar Links -->
		 <div id="navbar" class="navbar-collapse collapse" style="max-height: 744px;">
			 <?php
			 $args = array(
				 'theme_location' 	=> 'my-sub-menu',
				 'menu'        		=> 'Sub Pages Menu',
				 'menu_class'  		=> 'nav navbar-nav',
				 'container'   		=> false
				 );
 
			 wp_nav_menu( $args );
			 ?>
		 </div><!--/.navbar-collapse -->
	 </div><!-- / .container -->
 </nav>
<div id="t_pages" class="condominiums pagee">

	
	<?php		
	$args = array(
		'post_type' => 'sub_pages',
		'category_name' => 'condominium'
	);
	$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post(); ?>

		 <div class="banner" style="background-image: url(<?php the_field('banner_bg') ?>);">
				<img src="<?php the_field('banner_bg_inner'); ?>" alt="Project Example">
		 </div>
		 <div class="common-desc wow fadeInUp" style="width: 70%; padding-bottom: 1em;">
				<p><?php echo get_the_content(); ?></p>
		 </div>
	<?php
		}
		wp_reset_postdata();
	} else {
		// no posts found
		echo 'no found';
	} ?>


	 <div class="sec-heading">
			<h2>GALLERY</h2>
	 </div>




<?php
$categories = get_categories( array(
            'type'        => 'post',
            'child_of'    => 6,
            'parent'      => '',
            'orderby'     => 'name',
            'order'       => 'ASC',
            'hide_empty'  => false,
            'hierarchical'=> 1,
            'exclude'     => '',
            'include'     => '',
            'number'      => '',
            'taxonomy'    => 'category',
            'pad_counts'  => false 
        ) );
    ?>
    <ul id="gallery-cpt-filters" class="gallery-filters">
    <?php

    foreach( $categories as $sub ) {
    	echo '<li class=""><a href="#">'. $sub->name .'</a></li>';
    } ?>
    </ul>
	    <?php
	$count = 0;
   	foreach( $categories as $subitems ) {  $count++; ?>
		 <div class="gallery-items grid clearfix <?php if($count == 1){echo ' active';} ?>" style="margin-bottom: 6em;">

		 	<?php  if($subitems->category_count != 0) { ?>
			<?php
				 $args = array(
					 'post_type' => 'aeon_tower_gallery',
					 'category_name' => $subitems->slug
				 );
				 $query = new WP_Query( $args );
				 if ( $query->have_posts() ) {
				 while ( $query->have_posts() ) {
				 $query->the_post(); ?>
			<figure class="effect-winston">
				 <?php 
						$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large', false );
						$tmb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false );
						?>
				 <img src="<?php echo $tmb[0]; ?>" alt="img30">
				 <figcaption>
						<a href="<?php echo $src[0]; ?>" data-caption="<?php echo  the_title() . '<br>' . the_content(); ?>" data-fancybox="roadtrip-<?php echo $subitems->slug ?>-1">
							 <h2><?php the_title(); ?></h2>
						</a>
						<p>
							 <a href="<?php echo $src[0]; ?>" data-caption="<?php echo  the_title() . '<br>' . the_content(); ?>" data-fancybox="roadtrip-<?php echo $subitems->slug ?>-2">Read More...</a>
						</p>
				 </figcaption>
			</figure>
			<?php
			 }
			 wp_reset_postdata();
			 }
			} else {
				echo "<h1>No Gallery Found</h1>";
			}?>
		 </div>
   		<?php } ?>
	 



</div>
</div>
<?php get_footer(); ?>

