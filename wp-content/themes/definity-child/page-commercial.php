<?php
/*
Template Name: Commercial Page
Template Post Type: post, page, event
*/
// Page code here...
get_header();
?>
<nav class="nextNav active navbar navbar-default mega trans-helper navbar-trans navbar-trans-dark row">
	<div class="container">
		<div class="navbar-header">
			<button id="nav-icon1" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<!-- Logo -->
			<a class="navbar-brand" href="index.html">
				<img src="<?php echo site_url(). '/wp-content/uploads/2018/04/Aeon-Towers-logo-1.png'; ?>" alt="Definity - Logo">
			</a>
		</div><!-- / .navbar-header -->
		<!-- Navbar Links -->
		<div id="navbar" class="navbar-collapse collapse" style="max-height: 744px;">
			<?php
			$args = array(
				'theme_location' 	=> 'my-sub-menu',
				'menu'        		=> 'Sub Pages Menu',
				'menu_class'  		=> 'nav navbar-nav',
				'container'   		=> false
				);

			wp_nav_menu( $args );
			?>
		</div><!--/.navbar-collapse -->
	</div><!-- / .container -->
</nav>


<div id="commercial-page" class="pagee">

  <?php   
  $args = array(
    'post_type' => 'sub_pages',
    'category_name' => 'diningandoffice',
    'tag' => 'banner'
  );
  $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
      $the_query->the_post(); ?>


	<div class="banner" style="background-image: url(<?php the_field('banner_bg') ?>">

		<div class="container">
			<div class="container-inner">
				<h1 class="wow fadeInUp"><?php echo get_the_title(); ?></h1>
				<h2 class="wow fadeInUp"><?php the_field('banner_heading_sub') ?> </h2>
			</div>
		</div>
	</div>

  <?php
    }
    wp_reset_postdata();
  } else {
    // no posts found
    echo 'no found';
  } ?>
	<div class="container" style="padding-top: 60px;">
		<div class="cont-inner align-center text-center">
			<h2 class="wow fadeInUp">SPACES</h2>


			  <?php   
  $args = array(
    'post_type' => 'sub_pages',
    'category_name' => 'diningandoffice',
    'tag' => 'table',
    'order' => 'ASC'
  );
  $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
      $the_query->the_post(); 
      	the_content();
      ?>
			<div class="row">
				<p class="col-md-offset-3 col-md-6 wow fadeInUp">
					<a href="<?php the_field('banner_bg_inner');?>" data-fancybox="office"><img src="<?php the_field('banner_bg_inner');?>" alt=""></a>
				</p>
			</div>
  <?php
    }
    wp_reset_postdata();
  } else {
    // no posts found
    echo 'no found';
  } ?>


			<!-- <header class="sec-heading" style="padding-top: 30px; margin-bottom: 30px;">
				<h2>GALLERY</h2>
			</header>
			<div class="testimonials-3col">
				<?php
				$args = array(
				'post_type' => 'aeon_tower_gallery',
				'category_name' => 'diningandoffice'
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
				$query->the_post(); ?>
				<div class="col-md-4 mb-sm-50">
					<div class="t-item wow fadeIn">
						<div class="image-cont">
							<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large', false ); ?>
							<div class="image-desc">
								<img src="<?php echo $src[0]; ?>" alt="">
								<?php the_content(); ?>
								
							</div>
						</div>
						<div class="test-cont">
							<h2><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<?php
				}
				wp_reset_postdata();
				}
				?>
			</div> -->
		</div>
	</div>
</div>
<?php get_footer() ?>