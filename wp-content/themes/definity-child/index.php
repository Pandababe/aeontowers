<?php
get_header();
?>
<!-- ========== Navigation ========== -->
<div class="topnum"></div>
<nav class="mainNav active">
	<div class="container clearfix">
		<div class="logo-01">		
		<?php	
			$args01 = array(
				'post_type' => 'landing_page',
				'tag' => 'logo-1'
			);
			$the_query = new WP_Query( $args01 );
				if ( $the_query->have_posts() ) { $the_query->the_post();?>
					<a href="<?php the_field('navigate_page_url')?>">
						<img src="<?php the_field('landing_image');?>" alt="">
					</a>
		<?php
			
			/* Restore original Post Data */
			wp_reset_postdata();
			} else {
				// no posts found
				echo 'No Found';
			} ?>
		</div>
	
		<div class="logo-02">		
		<?php	
			$args02 = array(
				'post_type' => 'landing_page',
				'tag' => 'logo-2'
			);
			$the_query = new WP_Query( $args02 );
				if ( $the_query->have_posts() ) { $the_query->the_post();?>

					<a href="<?php the_field('navigate_page_url')?>">
						<img src="<?php the_field('landing_image');?>" alt="">
					</a>
				<?php
			
			/* Restore original Post Data */
			wp_reset_postdata();
			} else {
				// no posts found
				echo 'No Found';
			} ?>
			</div>
		</div>
</nav>
<nav class="nextNav navbar navbar-default mega trans-helper navbar-trans navbar-trans-dark row">
	<div class="container">
		<div class="navbar-header">
			<button id="nav-icon1" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<!-- Logo -->
			<a class="navbar-brand" href="index.html"><img src="<?php echo site_url(). '/wp-content/uploads/2018/04/Aeon-Towers-logo-1.png'; ?>" alt="Definity - Logo"></a>
		</div><!-- / .navbar-header -->
		<!-- Navbar Links -->
		<div id="navbar" class="navbar-collapse collapse">
		<?php
			$args = array(
				'theme_location' 	=> 'main-menu',
				'menu'        		=> 'Main Menu',
				'menu_class'  		=> 'nav navbar-nav',
				'container'   		=> false
				);

			wp_nav_menu( $args );
		?>
		</div><!--/.navbar-collapse -->
	</div><!-- / .container -->
</nav>




<?php	
	$args1 = array(
		'post_type' => 'landing_page',
		'tag' => 'landing_tagline'
	);
	$the_query = new WP_Query( $args1 );
		if ( $the_query->have_posts() ) {
	
	while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>

	<section id="home" class="main-demo-hero" style="background-image: url(<?php the_field('landing_image') ?>)">
	<!-- Hero Content -->
	<div class="hero-content-wrapper">
		<div class="hero-content">
			<div class="c-bd  wow fadeInUp">
				<h1 class="hero-lead">
				<?php echo get_the_content();?>

				<a href="#welcome" class="scroller scroller-dark">
					<div class="item">
						<div class="mouse m-2"></div>
					</div>
				</a>
				</h1>
			</div>
		</div>
	</div>
</section> 
<?php
	}
	/* Restore original Post Data */
	wp_reset_postdata();
} else {
	// no posts found
	echo '<section id="home">No Found</section>';
} ?>





<section id="welcome" class="ss-style-doublediagonal">
	<div class="column">
		<div class="container">
			<div class="row">

				<?php		$args2 = array(
					'post_type' => 'landing_page',
					'tag' => 'welcome_heading'
				); 
				$the_query = new WP_Query( $args2 );
				if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					?>
				<div class="col-md-12">
					<header class="sec-heading wow fadeInUp">
					<h2>
						<?php echo get_the_title(); ?>
					</h2>
					</header>
					<p class="common-desc wow fadeInUp">
							<?php echo get_the_content(); ?>
					</p>
				</div>
				<?php 
					}
					/* Restore original Post Data */
					wp_reset_postdata();
				} else {
					// no posts found
					echo 'no found';
				} ?>
			</div>
			<?php		
				$args3 = array(
					'post_type' => 'landing_page',
					'tag' => 'welcome_table'
				);
				$the_query = new WP_Query( $args3 );
									if ( $the_query->have_posts() ) {
					
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						 echo get_the_content();
					}
					/* Restore original Post Data */
					wp_reset_postdata();
				} else {
					// no posts found
					echo 'no found';
			} ?>


		</div>
	</div>
</section>
<section id="commercial">

<?php		
	$args4 = array(
		'post_type' => 'landing_page',
		'tag' => 'dining_heading'
	);
	$the_query = new WP_Query( $args4 );
		if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() )  {
	$the_query->the_post();
?>
	<div class="commercial-bg parallax" style="background-image: url(<?php the_field('landing_image') ?>)">
		<div class="container">

			<div class="row">
				<header class="sec-heading wow fadeInUp">
					<h2><?php echo get_the_title(); ?></h2>
				</header>
				<div class="common-desc col-md-offset-2 col-md-8 text-center ws-m wow fadeInUp" style="padding-top: 20px;">
					<?php echo get_the_content(); ?>
					<a href="<?php the_field('navigate_page_url')?>" class="btn wow fadeIn">Learn More</a>

				</div>
			</div>

		</div>


	</section>
	</div>
<?php }
	/* Restore original Post Data */
	wp_reset_postdata();
} else {
	// no posts found
	echo 'no found';
} ?>

	<?php		
		$args5 = array(
			'post_type' => 'landing_page',
			'tag' => 'dining_whats_new'
		);
		$the_query = new WP_Query( $args5 );
			if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post(); ?>

			<section class="ss-style-doublediagonal">
				<header class="sec-heading" style="padding-top: 60px; margin-bottom: 40px;">
				<h2>
				 <?php echo get_the_title(); ?>
				</h2>
				</header>
				<div class="common-desc">
				<?php echo get_the_content(); ?>
				</div>
			</section>
	<?php 	}
		/* Restore original Post Data */
		wp_reset_postdata();
	} else {
		// no posts found
		echo 'no found';
	} ?>

</section>
<section id="vivid">

	<div class="vivid-slider-container">
		<div class="vivid-tagline text-center wow fadeInUp">

		<?php		
		$args6 = array(
			'post_type' => 'landing_page',
			'tag' => 'hotel_tagline'
		);
		$the_query = new WP_Query( $args6 );
			if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post(); 
				echo get_the_content();
		/* Restore original Post Data */
			}
			wp_reset_postdata();
		} else {
			// no posts found
			echo 'no found';
		} ?>

		</div>
		<div class="vivid-slider">
		<?php		
		$args7 = array(
			'post_type' => 'hotel_slider',
			'order' => 'ASC'
		);
		$the_query = new WP_Query( $args7 );
			if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
			$the_query->the_post(); ?>
				<div style="background-color: #5e2944;"><img style="opacity: 0.4;" src="<?php the_field('hotel_slider_image');?>" alt=""></div>
			<?php }
				wp_reset_postdata();
			} else {
				// no posts found
				echo 'no found';
			} ?>
		</div>
		<div class="vivid-slider-pager">
			<p class="pager-prev"><i class="fa fa-angle-left"></i></p>
			<p class="pager-next"><i class="fa fa-angle-right"></i></p>
		</div>
	</div>
	<?php		
		$args8 = array(
			'post_type' => 'landing_page',
			'tag' => 'hotel_tagline'
		);
		$the_query = new WP_Query( $args8 );
			if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post(); ?>
				
				<div class="container">
		<div class="row">
			<div class="col-md-12 text-center wow fadeInUp vivid-logo">
				<img src="<?php the_field('landing_image')?>" alt="">
			</div>
		</div>
	</div>
	<?php
		/* Restore original Post Data */
			}
			wp_reset_postdata();
		} else {
			// no posts found
			echo 'no found';
		} ?>
	
</section>
<section id="vivid-2">
	<div class="container-fluid ft-hover-item">
		<div class="row">
			<div class="grid">


		<?php		
			$args9 = array(
				'post_type' => 'landing_page',
				'tag' => 'vivid_two_navigation',
				'order' => 'ASC'
			);
			$the_query = new WP_Query( $args9 );
				if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post(); ?>

					<div class="col-md-6 col-sm-6 wow fadeIn">
						<fieldset>
							<figure class="effect-chico">
								<img src="<?php the_field('landing_image')?>" alt="<?php echo get_the_title(); ?>">
								<figcaption>
								<div class="cont-f">
									<h2><span><?php echo get_the_title(); ?></span></h2>
									<p><?php echo get_the_content(); ?></p>
									
									<a class="btn" href="<?php the_field('navigate_page_url');?>">LEARN MORE</a>
								</div>
								</figcaption>
							</figure>
						</fieldset>
					</div>
		<?php
			}
			wp_reset_postdata();
		} else {
			// no posts found
			echo 'no found';
		} ?>

			</div>
		</div>
	</div>
</section>

<?php		
	$args10 = array(
		'post_type' => 'landing_page',
		'tag' => 'roof_top'
	);
	$the_query = new WP_Query( $args10 );
		if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post(); ?>

<section id="skydeck">
	<div class="common-banner" style="background-image: url(<?php the_field('landing_image') ?>);">
		<div class="banner-cont-com">
			<div class="cont-inner wow fadeInUp">
				<h2><?php echo get_the_title(); ?></h2>
				<a href="<?php the_field('navigate_page_url') ?>" class="btn wow fadeIn">Learn More</a>
			</div>
		</div>
	</div>
	<section class="ss-style-doublediagonal">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="common-desc wow fadeInUp">

						<?php echo get_the_content(); ?>

					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<?php
	}
	wp_reset_postdata();
} else {
	// no posts found
	echo 'no found';
} ?>

<?php		
	$args11 = array(
		'post_type' => 'landing_page',
		'tag' => 'condominium'
	);
	$the_query = new WP_Query( $args11 );
		if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post(); ?>

<section id="condominium">
	<div class="common-banner" style="background-image: url(<?php the_field('landing_image') ?>);">
		<div class="banner-cont-com">
			<div class="cont-inner wow fadeInUp">
				<h2><?php echo get_the_title(); ?></h2>
				<a href="<?php the_field('navigate_page_url') ?>" class="btn wow fadeIn">Learn More</a>
			</div>
			<p class="cont-tag wow fadeInUp"><?php the_field('sub_desc') ?></p>
		</div>
	</div>
	<section class="ss-style-doublediagonal">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="common-desc wow fadeInUp">

						<?php echo get_the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>


<?php
	}
	wp_reset_postdata();
} else {
	// no posts found
	echo 'no found';
} ?>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<header class="sec-heading">
					<h2 class="wow fadeInUp">CONTACT US</h2>
					<span class="subheading fadeInUp">WE LOVE TO HEAR FROM YOU</span>
				</header>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3858.615136413322!2d121.0576853150399!3d14.734337989717467!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32f96daaf81ab1ff%3A0xae3946925cee0ce8!2sAeon+Towers!5e0!3m2!1sen!2sph!4v1523724435271" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
	<div class="container contact-area">
		<div class="col-md-4">
			<?php		
		$args12 = array(
			'post_type' => 'landing_page',
			'tag' => 'contact'
		);
		$the_query = new WP_Query( $args12 );
			if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) {
				$the_query->the_post(); 
				echo get_the_content();
		}
		wp_reset_postdata();
	} else {
		// no posts found
		echo 'no found';
	} ?>
		</div>
			<div class="col-md-8" id="inquiry_tab">
				<div class="inq">
					<?php echo do_shortcode('[contact-form-7 id="261" title="Leasing Concern"]'); ?>
				</div>
				<div class="inq" style="display: none;">
					<?php echo do_shortcode('[contact-form-7 id="45" title="Condominium"]'); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
?>