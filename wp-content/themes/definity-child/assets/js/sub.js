jQuery(document).ready(function(){
    jQuery('#gallery-cpt-filters li, .gallery-items').first().addClass('active');
 	jQuery("#gallery-cpt-filters li a").click(function(e){
		e.preventDefault();
		jQuery(this).parent().addClass('active').siblings().removeClass('active');
		var id =  jQuery(this).parent().index();
		jQuery('.gallery-items').eq(id).addClass('active').siblings().removeClass('active');
	});
});