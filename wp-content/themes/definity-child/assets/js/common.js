jQuery(document).ready(function() {
	var $ = jQuery,
		$window = $(window),
		$document = $(document),
		docHeight = $document.innerHeight(),
		winWidth = $window.innerWidth(),
		winHeight = $window.innerHeight(),
		$header = $('.nextNav'),
		hh = $header.innerHeight(),
		scrolled = $(window).scrollTop();


	/**
	* --------------------------------------------------------------------------
	* EVENTS
	* --------------------------------------------------------------------------
	*/

	$window.on('resize', function(){
		updateOnResize();
	});

	var updateOnResize = debounce(function() {
		updateValueOnResize();
		updateStyleOnResize();
	}, 250);

	function updateValueOnResize() {
		winWidth = $window.innerWidth();
		winHeight = $window.innerHeight();
		hh = $header.innerHeight();
	}

	function updateStyleOnResize() {
		// ADD PADDING TOP TO CLASS PAGEE
		$(".pagee").css("padding-top", hh)
	}

	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};


	/**
	* --------------------------------------------------------------------------
	* ADD CLASS ON ALL IMAGE
	* --------------------------------------------------------------------------
	*/

	$("img").addClass("img-responsive");


	/**
	* --------------------------------------------------------------------------
	* TOGGLE RESPONSIVE BURGER MENU ICON
	* --------------------------------------------------------------------------
	*/

 	$('#nav-icon1').click(function() {
		$(this).toggleClass('open');
	});

});